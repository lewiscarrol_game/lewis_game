words = [word.strip() for word in open("sowpods.txt")]

def transform(start: str, end: str) -> list[str]:
    route = []
    if start == end:
        return route
    else:
        for i in range(len(start)):
            new_word = start[:i] + end[i] + start[i + 1:]
            if new_word in words:
                route.append(new_word)
                return transform(new_word, end)


print(transform("clay","gold"))
