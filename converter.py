steps = []
def common(choice: str, word: str):
            return len(set(choice) & set(word))
data = [word.strip() for word in open("sowpods.txt")]

words_data = [word.strip() for word in open("sowpods.txt")]

def modify(start: str, stop: str) -> list[str]:
    alphabets = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
    update_steps = [(start, [start])]
    

    while update_steps:
        current_word, path = update_steps.pop(0)
        print(current_word, path)
        if current_word == stop:
            path = " -> ".join(path)
            return f"Route is {path}"
        for i, char in enumerate(current_word):
            if char != stop[i]:
                new_word = current_word[:i] + stop[i] + current_word[i+1:]
                if new_word in words_data:
                    update_steps.append((new_word, path + [new_word]))
                else:
                    for candidate in alphabets:
                       if candidate != char:
                            new_word = current_word[:i] + candidate + current_word[i+1:]
                            if new_word in data:
                                if common(current_word, stop) == i + 1:
                                    update_steps.append((new_word, path + [new_word]))
                                    break
        
    return []
steps = modify('CLAY', 'GOLD')
print(steps)
